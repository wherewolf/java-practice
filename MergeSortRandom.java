/**
  * a simple implementation of merge sort
  * taken from geeksforgeeks.org
  * 
  * modified to generate an array of random integers of size 200
  *
  * @author: Daniel Jaeger
  * @version: 4/18/2019
  */

import java.util.Random;

public class MergeSortRandom{
	
	//merge two subarrays of arr[]
	//first subarray is arr[l...m]
	//second subarray is arr[m+1...r]
	void merge(int arr[], int l, int m, int r){
		//find sizes of two subarrays to be merged
		int n1 = m - l + 1;
		int n2 = r - m;

		//create temp arrays
		int L[] = new int[n1];
		int R[] = new int[n2];

		//copy data to temp arrays
		for(int i=0; i<n1; i++){
			L[i] = arr[l+i];
		}
		for(int j=0; j<n2; j++){
			R[j] = arr[m+1+j];
		}

		//merge temp arrays
		//initial indices of first and second subarrays
		int i = 0, j = 0;

		//intial index of merged subarray array
		int k = l;
		while(i<n1 && j<n2){
			if(L[i] <= R[j]){
				arr[k] = L[i];
				i++;
			}else{
				arr[k] = R[j];
				j++;
			}
			k++;
		}

		//copy remaining elements of L[] (if any exist)
		while(i<n1){
			arr[k] = L[i];
			i++;
			k++;
		}


		//copy remaining elements of R[] (if any exist)
		while(j<n2){
			arr[k] = R[j];
			j++;
			k++;
		}
	}

	//main function that sorts arr[l...r] using merge()
	void sort(int arr[], int l, int r){
		if(l<r){
			//find the mid-point
			int m = (l+r)/2;

			//sort first and scond halves
			sort(arr, l, m);
			sort(arr, m+1, r);

			//merge the sorted halves
			merge(arr, l, m, r);
		}
	}

	//a utility function to print array of size n
	static void printArray(int arr[]){
		int n = arr.length;
		int counter = 0;
		for(int i=0; i<n; i++){
			counter++;
			if(counter % 10 == 0){
				System.out.printf("%7d", arr[i]);
			 	System.out.println();
			}
			else if(counter % 10 == 1){
				System.out.printf("%4d", arr[i]);
			}
			else if(counter % 10 == 2){
				System.out.printf("%7d", arr[i]);
			}
			else if(counter % 10 == 3){
				System.out.printf("%7d", arr[i]);
			}
			else if(counter % 10 == 4){
				System.out.printf("%7d", arr[i]);
			}
			else if(counter % 10 == 5){
				System.out.printf("%7d", arr[i]);
			}
			else if(counter % 10 == 6){
				System.out.printf("%7d", arr[i]);
			}
			else if(counter % 10 == 7){
				System.out.printf("%7d", arr[i]);
			}
			else if(counter % 10 == 8){
				System.out.printf("%7d", arr[i]);
			}
			else if(counter % 10 == 9){
				System.out.printf("%7d", arr[i]);
			}
			//System.out.printf("%-4d %7d %7d %n", arr[i], arr[i+1], arr[i+2]);
		}
		System.out.println();
	}

	//drive method
	public static void main(String args[]){
		Random random = new Random();
		int randomInt;
		int arr[] =  new int[200];
		//int arr[] = {12, 11, 13, 5, 6, 7};

		for(int i=0; i<arr.length; i++){
			arr[i] = random.nextInt(10000);
		}

		System.out.println("Given Array");
		printArray(arr);

		int l = 0;
		int r = arr.length - 1;

		MergeSort object = new MergeSort();
		object.sort(arr, l, r);

		System.out.println("\nSorted Array");
		printArray(arr);		
	}
}