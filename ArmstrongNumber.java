/**
  * A program that takes in an integer from a user and tells the user whether or not said number 
  * is an Armstrong number. 
  *
  * An Armstrong number of three digits is an integer such that the sum of the cubes of its digits 
  * is equal to the number itself. 
  * For example, 371 is an Armstrong number because 3^3 + 7^3 + 1^3 = 371.
  */

import java.util.Scanner;

public class ArmstrongNumber{
	static Scanner input = new Scanner(System.in);

	public static int sum(int num){
		int mod = 0;
		int sum = 0;
		while(num != 0){
			mod = num % 10;
			mod = mod * mod * mod;
			num = num / 10;
			sum = sum + mod;
		}
		//System.out.println("" + sum);
		return sum;
	}

	public static void ArmstrongCheck(int num){
		int check = sum(num);
		String yes = "" + num + " is an Armstrong Number!";
		String no = "" + num + " is not an Armstrong Number!";
		if (check == num){
			System.out.println(yes);
		} else {
			System.out.println(no);
		}
	}

	public static void main(String[] args){
		System.out.println("This program checks whether or not a number is an Armstrong Number.");
		int numCheck; 

		do {
			System.out.println();
			System.out.println("Enter an integer.");
			System.out.println("Enter 0 to exit the program.");
			System.out.println();
			numCheck = input.nextInt();
			ArmstrongCheck(numCheck);
		} while (numCheck != 0);
	}
}