/**
  * a simple implementation of insertion sort
  * taken from geeksforgeeks.org
  *
  * @author: Daniel Jaeger
  * @version: 4/18/2019
  */

public class InsertionSort{
	//method to sort array using insertion sort
	void sort(int arr[]){
		int n = arr.length;
		for(int i = 1; i < n; i++){
			int key =  arr[i];
			int j = i - 1;
			System.out.printf("i = %d\n", i);
			System.out.printf("key = %d\n", arr[i]);
			System.out.printf("j = %d\n", j);


			//moves elements of arr[0...i-1] that are greater
			//than the key to one position ahead of their 
			//current position
			while(j >= 0 && arr[j] > key){
				System.out.printf("arr[j] = %d\n", arr[j]);
				System.out.printf("key = %d\n", key);
				System.out.printf("j >= 0 & arr[j] > key, so...\n");

				arr[j+1] = arr[j];
				j--;
				System.out.printf("arr[j+1] = arr[j] = %d\n", arr[j+1]);
				System.out.printf("j = %d\n", j);
			}
			arr[j+1] = key;
			System.out.printf("Either j < 0 or arr[j] <= key, so now arr[j+1] = key\n");
			System.out.printf("therefore, arr[j+1] = %d\n", arr[j+1]);
			System.out.println("Current State of Array:");
			printArray(arr);
			System.out.println();
		}
	}

	//helper method
	//prints the array
	static void printArray(int arr[]){
		int n = arr.length;
		for(int i = 0; i < n; i++){
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

	//driver method
	public static void main(String args[]){
		int arr[] = {12, 11, 13, 5, 6};

		System.out.println("Given Array:");
		printArray(arr);

		InsertionSort object = new InsertionSort();
		object.sort(arr);

		System.out.println();
		System.out.println("Sorted Array:");
		printArray(arr);
	}

}