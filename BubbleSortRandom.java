/**
  * a simple implementation of bubble sort
  * taken from geeksforgeeks.org.
  * 
  * modified to generate an array of random integers of size 200
  *
  * @author: Daniel Jaeger
  * @version: 4/18/2019
  */

import java.util.Random;

public class BubbleSortRandom{
	
	void bubbleSort(int arr[]){
		int n = arr.length;
		for(int i=0; i<n-1; i++){
			for(int j=0; j<n-i-1; j++){
				if(arr[j] > arr[j+1]){
					//swap arr[j+1] and arr[i]
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
	}

	//helper method
	//prints the array
	static void printArray(int arr[]){
		// int n = arr.length;
		// for(int i=0; i<n; i++){
		// 	System.out.print(arr[i] + " ");
		// }
		// System.out.println();
		int n = arr.length;
		int counter = 0;
		for(int i=0; i<n; i++){
			counter++;
			if(counter % 10 == 0){
				System.out.printf("%7d", arr[i]);
			 	System.out.println();
			}
			else if(counter % 10 == 1){
				System.out.printf("%4d", arr[i]);
			}
			else if(counter % 10 == 2){
				System.out.printf("%7d", arr[i]);
			}
			else if(counter % 10 == 3){
				System.out.printf("%7d", arr[i]);
			}
			else if(counter % 10 == 4){
				System.out.printf("%7d", arr[i]);
			}
			else if(counter % 10 == 5){
				System.out.printf("%7d", arr[i]);
			}
			else if(counter % 10 == 6){
				System.out.printf("%7d", arr[i]);
			}
			else if(counter % 10 == 7){
				System.out.printf("%7d", arr[i]);
			}
			else if(counter % 10 == 8){
				System.out.printf("%7d", arr[i]);
			}
			else if(counter % 10 == 9){
				System.out.printf("%7d", arr[i]);
			}
			//System.out.printf("%-4d %7d %7d %n", arr[i], arr[i+1], arr[i+2]);
		}
		System.out.println();
	}

	//driver method to test above implementation
	public static void main(String args[]){
		Random random = new Random();
		int randomInt;
		int arr[] = new int[200];

		for(int i=0; i<arr.length; i++){
			arr[i] = random.nextInt(10000);
		}

		System.out.println("Given Array");
		printArray(arr);

		BubbleSortRandom object = new BubbleSortRandom();
		//int arr[] = {64, 34, 25, 12, 22, 11, 90};
		object.bubbleSort(arr);

		System.out.println("Sorted Array:");
		object.printArray(arr);
	}
}