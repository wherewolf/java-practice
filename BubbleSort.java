/**
  * a simple implementation of bubble sort
  * taken from geeksforgeeks.org
  *
  * @author: Daniel Jaeger
  * @version: 4/18/2019
  */

public class BubbleSort{
	
	void bubbleSort(int arr[]){
		int n = arr.length;
		for(int i=0; i<n-1; i++){
			System.out.printf("i = %d\n", i);
			printArray(arr);

			for(int j=0; j<n-i-1; j++){
				System.out.printf("i = %d\n", i);
				System.out.printf("j = %d\n", j);
				printArray(arr);
				if(arr[j] > arr[j+1]){
					//swap arr[j+1] and arr[i]
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
					System.out.println("Array after swap:");
					printArray(arr);
				}
			}
		}
	}

	//helper method
	//prints the array
	void printArray(int arr[]){
		int n = arr.length;
		for(int i=0; i<n; i++){
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

	//driver method to test above implementation
	public static void main(String args[]){
		BubbleSort object = new BubbleSort();
		int arr[] = {64, 34, 25, 12, 22, 11, 90};
		object.bubbleSort(arr);
		System.out.println("Sorted Array:");
		object.printArray(arr);
	}
}