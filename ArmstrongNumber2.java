/**
  * A program that takes in an integer from a user and tells the user every Armstrong number between 0 
  * and the integer the user entered.
  *
  * An Armstrong number of three digits is an integer such that the sum of the cubes of its digits 
  * is equal to the number itself. 
  * For example, 371 is an Armstrong number because 3^3 + 7^3 + 1^3 = 371.
  */

import java.util.Scanner;

public class ArmstrongNumber2{
	static Scanner input = new Scanner(System.in);
	static int counter = 0;
	public static int sum(int num){
		int mod = 0;
		int sum = 0;
		while(num != 0){
			mod = num % 10;
			mod = mod * mod * mod;
			num = num / 10;
			sum = sum + mod;
		}
		//System.out.println("" + sum);
		return sum;
	}

	public static void ArmstrongCheck(int num){
		int check = sum(num);
		String yes = "" + num + " is an Armstrong Number!";
		if (check == num){
			System.out.println(yes);
			counter++;
		} 
	}

	public static void main(String[] args){
		System.out.println("This program every number that is an Armstrong Number between 0 and whatever integer you enter.");
		System.out.println("Enter an integer greater than 0.");

		long number = (long) input.nextInt();
		int total = 0;

		for (int i = 0; i <= number; i++){
			ArmstrongCheck(i);
		} 
		System.out.println("There are " + counter + " Armstrong Numbers between 0 and " + number);
	}
}